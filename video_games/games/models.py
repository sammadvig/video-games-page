from django.db import models


# Create your models here.
class games(models.Model):
    title = models.CharField(max_length=50)
    developer = models.CharField(max_length=50)
    picture = models.URLField()
    release_date = models.DateField()
