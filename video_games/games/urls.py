from django.urls import path
from games.views import games_list


urlpatterns = [
    path("list/", games_list, name="games_list")
]
