from django.shortcuts import render


# Create your views here.
def games_list(request):
    return render(request, "games/list.html")
